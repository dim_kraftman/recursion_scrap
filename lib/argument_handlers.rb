module ArgumentHandlers

  def get_domain_list(domain_file)
    if !File.exist? domain_file
      raise RuntimeError, "Файл #{domain_file} не существует"
    else
      domain_list = File.readlines(domain_file).map(&:chomp).reverse
      if domain_list.size === 0
        raise RuntimeError, "Пустой список доменов"
      end
    end
    domain_list
  end

  def get_proxy_list(proxy_file)
    if !File.exist? proxy_file
      raise RuntimeError, "Файл #{proxy_file} не существует"
    else
      proxy_list = File.readlines(proxy_file).map(&:chomp)
      if proxy_list.size === 0
        raise RuntimeError, "Пустой список прокси"
      end
    end
    proxy_list
  end
  
  def check_output_dir(output_dir)
    if !Dir.exist? output_dir
      begin
        FileUtils.mkdir_p output_dir
      rescue
        raise "Ошибка создания директории #{output_dir}"
      end
    end
    true
  end
  
  def create_error_log(error_log_file)
    if !File.exists? error_log_file
      begin
        FileUtils.touch error_log_file
      rescue
        raise "Ошибка создания файла лога #{error_log_file}"
      end
    end
    true
  end
  

end