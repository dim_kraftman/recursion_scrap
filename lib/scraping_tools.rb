module ScrapingTools

      # Возвращает текст из html страницы
      def collect_text(html)
        string_array =[]
  
        text = html.search('//text()')
        text.each do |text|
          string_array << text.to_s.strip! unless text.to_s.strip!.nil?
        end
        string_array.compact!
        unless string_array.empty?
          result = string_array.uniq!.join("\n")
        else
          result = ""
        end
        result
      end
  
       # пишем собранный текст в файл
      def write_text_to_file(text, uri)
        File.open("#{OUTPUT_DIR}/#{uri.host}.txt", mode: "a") do |file|
          file << text
        end
      end
      
      # приводим ссылки к стандартному виду
      def normalize_links(links, uri)
        links = links.compact.uniq
        links = links.map{|link| link unless link.match("^#")}.compact.uniq
        links = links.map{|link| link unless link.match("^//")}.compact.uniq
        links = links.map{|href| ((href.match("^/")) ? "#{uri}#{href}" : href)}
        links = links.compact.uniq
        links = links.map{|href| URI((href.match("^http")) ? href : "#{uri}#{href}")}
        links.delete(uri)
        links
      end

      # Собираем ссылки со страницы
      def collect_links(html, uri)
        links = []
        html.css('a').each do |a|
          links << a["href"].to_s
        end
        links = normalize_links(links, uri)
      end

      # Возвращает html страницы
      def get_html_from_url(browser, uri)
        begin
          browser.goto uri.to_s
        rescue Ferrum::TimeoutError
          puts "!!!!! Achtung ! Timeout Error in " + uri.to_s
        rescue Ferrum::StatusError
          puts "!!!!! Achtung ! Status Error in " + uri.to_s
        rescue StandardError => e
          puts "!!!!! Achtung ! Sandart Error in " + uri.to_s
          puts e.message
        end
        html = Nokogiri::HTML browser.body
        html.css("script").remove
        html.css("style").remove
        html
      end

      # метод рекурсивного прохождения по ссылкам
      def recursive_scrap(uri, browser, depth)
        result = []
        links = []

        # Извлекаем код HTML страницы из полученного uri
        html = get_html_from_url(browser, uri)

        #Извлекаем текст из полученного html и пишем в файл
        text = collect_text(html)
        write_text_to_file(text, uri)
        puts "Записали текст полученный по ссылке: " + uri.to_s if DEBUG

        if depth > 0
          #Извлекаем ссылки из полученного html если текущий уровень рекурсии > 0
          puts ("collect links from page " + uri.to_s) if DEBUG
          links = collect_links(html, uri)
          puts "Всего ссылок на странице: " + links.size.to_s if DEBUG
          puts "Уровень рекурсии: " + depth.to_s if DEBUG
          links.each do |link|
            puts "Переходим рекурсивно по ссылке: " + link.to_s if DEBUG
            result << recursive_scrap(link, browser, depth - 1)
          end
        end
        result
      end
end