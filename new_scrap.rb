# frozen_string_literal: true


require 'fileutils'
require 'ferrum'
require 'nokogiri'
require './lib/argument_handlers'
require './lib/scraping_tools'

include ArgumentHandlers
include ScrapingTools

if (ARGV[0] == '--help')

  puts <<-HELP
  Запуск: ruby new_scrap.rb ARGS...
  Аргументы:
        --in         - Список доменов (по умолчанию ./in/domains.list)
        --proxy      - Список прокси серверов (по умолчанию ./proxy/proxy.list)
        --proxy-type - Тип прокси
        --out        - Директория для вывода результатов
        --error-log  - Файл для вывода ошибок
        --threads    - Количество потоков
        --depth      - Глубина рекурсии при переходе по ссылкам с основной страницы (по умолчанию 1 )
        --help       - Список аргументов
  HELP
  exit;
  
end

if (ARGV.size % 2) != 0
  raise RuntimeError, "Недопустимый формат аргументов"
end

argv = Hash[*ARGV]

DEBUG           = true

MAX_FAILED      = 3
MAX_DRV_ATTEMPT = 3
TIMEOUT         = 10

THREADS         = argv["--threads"] || 2
OUTPUT_DIR      = argv["--out"] || "out"
PROXY_TYPE      = argv["--proxy-type"]
ERROR_LOG       = argv["--error-log"] || "log/error-#{Time.now.strftime("%d.%m.%Y")}.log"
DEPTH           = argv["--depth"] || 1 

domain_file     = argv["--in"] || "domains.list"
proxy_file      = argv["--proxy"] || "proxy.list"

$domain_list    = []
$proxy_list     = []
$errors         = []



depth = DEPTH.to_i #глубина рекурсии
domain_file = "in/" + domain_file
proxy_file  = "proxy/" + proxy_file
domain_list = []

domain_list = get_domain_list(domain_file)
proxy_list  = get_proxy_list(proxy_file)
check_output_dir(OUTPUT_DIR)
create_error_log(ERROR_LOG)

    # TODO добавить многопоточность
    def gogogo(domain_list, depth, proxy_list)

      opts = { timeout: TIMEOUT, process_timeout: TIMEOUT, browser_options: { 'no-sandbox': nil } }
      browser = Ferrum::Browser.new opts

      domain_list.each do |url|
        uri = URI((url.match /http/) ? url : "http://#{url}")
        recursive_scrap(uri, browser, depth)
      end
      browser.quit
    end

    gogogo(domain_list, depth, proxy_list)


